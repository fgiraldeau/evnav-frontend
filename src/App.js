import React, { Component } from 'react';
import AppControls from './components/AppControls';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import primary from '@material-ui/core/colors/blue';
import secondary from '@material-ui/core/colors/orange';

const theme = createMuiTheme({
  palette: {
    primary,
    secondary,
  },
  status: {
    danger: 'red',
  },
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <AppControls></AppControls>
      </MuiThemeProvider>
    );
  }
}

export default App;
