import carData from './cars.json'
import tripDemo from './trip-01.json'

const Resources = {
    carData,
    tripDemo,
};

export default Resources;