import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import Slider from '@material-ui/lab/Slider';
import { store, collect } from 'react-recollect';

import Resources from '../res/Resources';

const styles = theme => ({
  formControl: {
    minWidth: 120,
    display: 'flex',
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const carItems = Resources.carData.map((item) => {
  const text = item.maker + " " + item.model + " (" + item.battTotal + " kWh)"
  const value = item.id;
  return { text, value };
});

const carMenuList = carItems.map((item) => {
  return (
    <MenuItem key={item.value} value={item.value}>{item.text}</MenuItem>
  );
});

class TripForm extends React.Component {

  componentWillMount = () => {
    store.carId = 0;
    store.efficiency = 180;
    store.soc = 100;
  }

  handleCarChange = (event) => {
    store.carId = event.target.value;
  };

  handleEfficiencyChange = (event, value) => {
    store.efficiency = value;
  };

  handleSOCChange = (event, value) => {
    store.soc = value;
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Typography variant="title" gutterBottom>
          Trip information
        </Typography>
        <Grid container spacing={24}>
          <Grid item xs={12} md={6}>
            <TextField required id="geoSource" label="Start" fullWidth />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField required id="geoDest" label="Destination" fullWidth />
          </Grid>
          <Grid item xs={12} md={12}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="car-select">Select your vehicule</InputLabel>
              <Select
                value={store.carId}
                onChange={this.handleCarChange}
                inputProps={{
                  name: 'carId',
                  id: 'car-select',
                }}
              >
              {carMenuList}
              </Select>
            </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <Typography id="efficiency-slider">Efficiency: {Number(store.efficiency).toFixed(0)} Wh/km</Typography>
              <Slider
                value={store.efficiency}
                min={80} max={300}
                aria-labelledby="efficiency-slider"
                onChange={this.handleEfficiencyChange} />
            </Grid>
            <Grid item xs={12} md={12}>
              <Typography id="soc-slider">State of charge: {Number(store.soc).toFixed(0)} %</Typography>
              <Slider
                value={store.soc}
                min={0} max={100}
                aria-labelledby="soc-slider"
                onChange={this.handleSOCChange} />
            </Grid>
            <Grid item xs={12} md={12}>
              <Button variant="contained" color="primary" fullWidth={true}>
                  Go!
              </Button>
            </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

TripForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(collect(TripForm));