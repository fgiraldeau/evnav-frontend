import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import EvStationIcon from '@material-ui/icons/EvStation';
import { collect, store } from 'react-recollect';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
  },
});

// Default value
store.loading = true;

class ChargerList extends React.Component {

  componentWillMount() {
    setTimeout(() => {
      this.props.store.loading = false;
    }, 3000);
  }

  render() {
    const { classes, chargers, store } = this.props;
    console.log("store.loading: " + store.loading)
    if (store.loading === true) {
      return <div>loading</div>;
    }
    
    return (
      <React.Fragment>
        <Typography variant="title" gutterBottom className={classes.root}>
          Optimal charging steps
        </Typography>
        <List>
          {chargers.map((charger, index) => {
            const s1 = Number(charger.charging_duration / 60).toFixed(0) + "min";
            const s2 = Number(charger.energy).toFixed(1) + "kWh";
            const s3 = Number(charger.charging_cost).toFixed(2) + "$";
            const details = s1 + " " + s2 + " " + s3;

            return (
              <ListItem key={index}>
                <EvStationIcon color="secondary" fontSize="large" />
                <ListItemText primary={charger.name} secondary={details} />
              </ListItem>
            );
          })}
        </List>
      </React.Fragment>
    );
  }
}

ChargerList.propTypes = {
  classes: PropTypes.object.isRequired,
  chargers: PropTypes.array.isRequired,
};

export default collect(withStyles(styles)(ChargerList));
