import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import TripForm from './TripForm';
import ChargerList from './ChargerList';
import Resources from '../res/Resources';
import { collect } from 'react-recollect';

const appWidth = 600;

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(appWidth + theme.spacing.unit * 2 * 2)]: {
      width: appWidth,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(appWidth + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3,
    },
  },
  stepper: {
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`,
  }
});

class AppControls extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <AppBar position="absolute" color="primary" className={classes.appBar}>
          <Toolbar>
            <Typography variant="title" color="inherit" noWrap>
              Electric Vehicule Navigation
            </Typography>
          </Toolbar>
        </AppBar>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <React.Fragment>
              <TripForm />
              <ChargerList chargers={Resources.tripDemo.charging_steps}/>
            </React.Fragment>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

AppControls.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(collect(AppControls));
