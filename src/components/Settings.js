import React from 'react';
import {
  Dropdown,
  Grid,
} from 'semantic-ui-react'
import 'rc-slider/assets/index.css';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import { store, collect } from 'react-recollect';

const handle = (props) => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Slider.Handle value={value} {...restProps} />
    </Tooltip>
  );
};

const Settings = ({ carData, efficiency }) => {

  const cityOptions = [
    { text: "Montreal", value: 0 },
    { text: "Quebec", value: 1 },
  ];

  const carOptions = carData.map((item) => {
    const text = item.maker + " " + item.model + " (" + item.battTotal + " kWh)"
    const value = item.id;
    return { text, value };
  });

  return (
    <div>
    <Grid>
      <Grid.Row>
        <Grid.Column verticalAlign='middle' width={2}>
        <label>From</label>
        </Grid.Column>
        <Grid.Column verticalAlign='middle' width={14}>
        <Dropdown
          selection
          options={cityOptions}
          defaultValue={0}
          fluid/>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
      <Grid.Column verticalAlign='middle' width={2}>
      <label>To</label>
      </Grid.Column>
      <Grid.Column verticalAlign='middle' width={14}>
      <Dropdown
        selection
        options={cityOptions}
        defaultValue={1}
        fluid/>
      </Grid.Column>
      </Grid.Row>
      <Grid.Row>
      <Grid.Column verticalAlign='middle' width={2}>
        <label>Car</label>
        </Grid.Column>
        <Grid.Column verticalAlign='middle' width={14}>
        <Dropdown
          selection
          options={carOptions}
          defaultValue={carOptions[0].value}
          fluid/>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column verticalAlign='middle' width={2}>
          <label>Efficiency {efficiency} Wh/km</label>
        </Grid.Column>
        <Grid.Column width={14}>
          <Slider min={80} max={300} defaultValue={180} handle={handle}/>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    </div>
  );
}

export default collect(Settings);
